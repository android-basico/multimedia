package com.example.multimedia;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class GrabadoraAudio extends AppCompatActivity {

    private MediaRecorder grabacion;
    private String archivoSalida = null;
    private Button btn_recorder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grabadora_audio);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btn_recorder = (Button) findViewById(R.id.btn_grabar);

        //verificando si los permisos estan en el archivo manifest (permisos de escritura en el dispositivo y grabacion de audio)
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            //Si se verifica que los permisos esta configurados, ENTONCES mostramos las ventanas emergentes para solicitar permisos al usuario
            ActivityCompat.requestPermissions(GrabadoraAudio.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, 1000);
        }
    }
    public void Grabar(View view) {
        //si es null no se esta grabando nada
        if (grabacion == null) {
            //creamos el path del archivo donde se grabara el audio
            archivoSalida = Environment.getExternalStorageDirectory().getAbsolutePath() + "/miGrabacion.mp3";
//            archivoSalida = Environment.getExternalStorageDirectory().getAbsolutePath() + "/miGrabacion.3gp";
            //capturando audio
            grabacion = new MediaRecorder();
            grabacion.setAudioSource(MediaRecorder.AudioSource.MIC);
            //formato de salida del audio (el formato no afecta con la anterior configuración)
            grabacion.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            //transformado el audio capturado
            grabacion.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
            //almacenando audio
            grabacion.setOutputFile(archivoSalida);
            try {
                //iniciando la grabación.
                grabacion.prepare();
                grabacion.start();
            }catch (IOException e) {
                e.printStackTrace();
            }

            btn_recorder.setBackgroundResource(R.drawable.grabadora_rec);
            Toast.makeText(this, "Grabando...", Toast.LENGTH_SHORT).show();
        } else if (grabacion != null) {
            // si se esta grabando los detenemos
            grabacion.stop();
            grabacion.release();
            grabacion = null;
            btn_recorder.setBackgroundResource(R.drawable.grabadora_stop_rec);
            Toast.makeText(this, "Grabacion finalizada", Toast.LENGTH_SHORT).show();
        }
    }
    public void reproducir(View view) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            //obteniendo audio grabado
            mediaPlayer.setDataSource(archivoSalida);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        Toast.makeText(this, "Reproduciendo audio", Toast.LENGTH_SHORT).show();
    }

}
