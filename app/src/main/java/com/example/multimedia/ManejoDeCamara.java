package com.example.multimedia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ManejoDeCamara extends AppCompatActivity {
    private ImageView imagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manejo_de_camara);
        imagen = (ImageView) findViewById(R.id.iv_camara);
//      El siguiente codigo no servira para mostrar al usuario los permisos necesarios
        if (ContextCompat.checkSelfPermission(ManejoDeCamara.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ManejoDeCamara.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ManejoDeCamara.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1000);
            System.out.println("OKKKKKKKKKKK");
        }

    }
//    variable que almacen el path de la imagen
    String currentPhotoPath;

    /***
     * Funcion encargada de crear el nombre y el archivo.
     * @return
     * @throws IOException
     */
    private File crearArchivoImagen() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Backup_" + timeStamp + "_";
//      Creara la imagen en el archivo creadio en file_paths.xml
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Guardando la ruta de la imagen
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }



    /***
     * Método para tomar la fotografia
     * @param view Activity
     */
    static final int REQUEST_TAKE_PHOTO = 1;
    public void tomarFoto(View view) {
        //Indica al dispositivo que se cerrara por un momento la activity actual
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Verifica y hay un Activity de camara o no se haya tomado una fotografia
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File archivoFotografia = null;
            try {
                //recupera el nombre de la imagen
                archivoFotografia = crearArchivoImagen();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (archivoFotografia != null) {
                //Creando el archivo
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        archivoFotografia);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    /***
     * Método para la vista previa
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imagen.setImageBitmap(imageBitmap);
        }
    }


}
