package com.example.multimedia;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.security.PublicKey;

public class MainActivity extends AppCompatActivity {

    Button play;
    SoundPool sp;
    int sonidoDeReproduccion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // IMPLEMNETADO AUDIO CON SOUNDPOOL PARA AUDIOS CORTOS MAXIMO DE 1MB
        play = (Button) findViewById(R.id.btn_uno);
        sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 1);

        //cargar pista de audio
        sonidoDeReproduccion = sp.load(this, R.raw.sound_short, 1);
    }
    public void audioSoundPool(View view) {
        //reproducir continuamente al presionar el boton
        sp.play(sonidoDeReproduccion, 1, 1, 1, -1, 0);
        //reproducir un vez al presionar el boton
        // sp.play(sonidoDeReproduccion, 1, 1, 1, 0, 0);
    }

    /***
     * Implementando reproducción de audios largos
     * @param view activity
     */
    public void audioMediaPlayer(View view) {
        MediaPlayer mp = MediaPlayer.create(this, R.raw.titanic);
        mp.start();
    }

    /***
     * Implementando reproductor de música
     * @param view activity
     */
    public void reproductorAudio(View view) {
        Intent reproductor = new Intent(this, ReproductorAudio.class);
        startActivity(reproductor);
    }
    /***
     * Implementando grabador de audio.
     * @param view activity
     */
    public void grabadoraAudio(View view) {
        Intent grabador = new Intent(this, GrabadoraAudio.class);
        startActivity(grabador);
    }

    /***
     * Implementando manejador de la camara del dispositivo.
     * @param view activity
     */
    public void capturarCamara(View view) {
        Intent camara = new Intent(this, ManejoDeCamara.class);
        startActivity(camara);
    }

    /***
     * Metodo para mostrar u ocultar los items de menu, el nombre del método debe ser onCreateOptionsMenu
     * @param menu, referencia al Menu
     * @return boolean, por defencto true para que cada vez que se presione el boton despliege el menu,
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nombre_menu_overflow, menu);
        return true;
    }

    /***
     * Método para asignar funciones a las opciones del menú, el nombre del método debe ser onOptionsItemSelected
     * @param menuItem
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // verificando que item se pulso
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.item1:
                Toast.makeText(this, "Presionaste la opción 1", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item2:
                Toast.makeText(this, "Presionaste la opción 2", Toast.LENGTH_SHORT).show();
                break;
            case R.id.item3:
                Toast.makeText(this, "Presionaste la opción 3", Toast.LENGTH_SHORT).show();
                break;
            case R.id.buscar:
                Toast.makeText(this, "Presionaste la acción del boton buscar", Toast.LENGTH_SHORT).show();
                break;
            case R.id.compartir:
                Toast.makeText(this, "Presionaste la acción del boton compartir", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
