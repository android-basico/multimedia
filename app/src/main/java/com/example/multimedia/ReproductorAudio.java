package com.example.multimedia;

import android.media.MediaPlayer;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ReproductorAudio extends AppCompatActivity {

    private Button btn_reproducir, btn_repetir;
    private MediaPlayer mp;
    ImageView iv;
    boolean repetir = false;
    int posicion = 0;

    MediaPlayer vectorMp[] = new MediaPlayer[3];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reproductor_audio);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btn_reproducir = (Button) findViewById(R.id.btn_reproducir);
        btn_repetir = (Button) findViewById(R.id.btn_repetir);
        iv = (ImageView) findViewById(R.id.imageView);
        vectorMp[0] = MediaPlayer.create(this, R.raw.race);
        vectorMp[1] = MediaPlayer.create(this, R.raw.sound);
        vectorMp[2] = MediaPlayer.create(this, R.raw.tea);
    }
    // Metodo para el play pause
    public void reprodicirDetener(View view) {
        //Saber si la pista de audio se esta reproduciendo
        if (vectorMp[posicion].isPlaying()) {
            // si se esta reproduciendo los pausamos
            vectorMp[posicion].pause();
            btn_reproducir.setBackgroundResource(R.drawable.reproducir);
            Toast.makeText(this, "Pausa", Toast.LENGTH_SHORT).show();
        } else {
            // si se esta reproduciendo los pausamos
            vectorMp[posicion].start();
            btn_reproducir.setBackgroundResource(R.drawable.pausa);
            Toast.makeText(this, "Reproducir", Toast.LENGTH_SHORT).show();
        }
    }
    //Metodo para detener la reproducción
    public void detener(View view) {
        //Verificando que hay una cancion en curso
        if (vectorMp[posicion] != null) {
            vectorMp[posicion].stop();
            // El metodo stop esta vaciando la posicion actual entonces debemos inicializar una vez las muestra lista
            vectorMp[0] = MediaPlayer.create(this, R.raw.race);
            vectorMp[1] = MediaPlayer.create(this, R.raw.sound);
            vectorMp[2] = MediaPlayer.create(this, R.raw.tea);
            posicion = 0;
            btn_reproducir.setBackgroundResource(R.drawable.reproducir);
            iv.setImageResource(R.drawable.portada1);
            Toast.makeText(this, "Detener", Toast.LENGTH_SHORT).show();
        }
    }
    // Metodo para repetir la pista
    public void repetir(View view) {
        if (repetir) {
            btn_repetir.setBackgroundResource(R.drawable.no_repetir);
            Toast.makeText(this, "No repetir la cancion", Toast.LENGTH_SHORT).show();
            vectorMp[posicion].setLooping(false);
            repetir = false;
        } else {
            btn_repetir.setBackgroundResource(R.drawable.repetir);
            Toast.makeText(this, "Repetir la cancion", Toast.LENGTH_SHORT).show();
            vectorMp[posicion].setLooping(false);
            repetir = true;
        }
    }
    //Metodo para ir a la siguiente pista
    public void siguiente(View view) {
        if (posicion < vectorMp.length -1) {
            if (vectorMp[posicion].isPlaying()) {
                vectorMp[posicion].stop();
                posicion++;
                vectorMp[posicion].start();
                if (posicion == 0) {
                    iv.setImageResource(R.drawable.portada1);
                } else if (posicion == 1) {
                    iv.setImageResource(R.drawable.portada2);
                } else if (posicion == 2) {
                    iv.setImageResource(R.drawable.portada3);
                }
            } else {
                posicion ++;
            }
        } else {
            Toast.makeText(this, "No hay mas canciones", Toast.LENGTH_SHORT).show();
        }
    }
    //Metodo para ir a la anterior pista
    public void anterior(View view) {
        if (posicion > 0) {
            if (vectorMp[posicion].isPlaying()) {
                vectorMp[posicion].stop();
                //Inicializar por si se pierde la posicion del vector
                vectorMp[0] = MediaPlayer.create(this, R.raw.race);
                vectorMp[1] = MediaPlayer.create(this, R.raw.sound);
                vectorMp[2] = MediaPlayer.create(this, R.raw.tea);
                posicion--;
                if (posicion == 0) {
                    iv.setImageResource(R.drawable.portada1);
                } else if (posicion == 1) {
                    iv.setImageResource(R.drawable.portada2);
                } else if (posicion == 2) {
                    iv.setImageResource(R.drawable.portada3);
                }
                vectorMp[posicion].start();
            } else {
                posicion --;
            }
        } else {
            Toast.makeText(this, "No hay mas canciones", Toast.LENGTH_SHORT).show();
        }
    }

}
